# Copyright (c) 2019-2021 Oracle and/or its affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at http://oss.oracle.com/licenses/upl.
#

output "jdetrial_public_ip" {
  value = format("You can connect to your JDE Trial instance: ssh -i %s opc@%s", var.private_key_path,oci_core_instance.jdetrial_vm.public_ip)
}


output "comments" {
  value = format("Your JDE Trial environment will be available in a few minutes at the following URL: http://%s:8079/jde", oci_core_instance.jdetrial_vm.public_ip)
}


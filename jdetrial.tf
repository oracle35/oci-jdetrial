resource "oci_marketplace_accepted_agreement" "jdetrial_mkplace_accepted_agreement" {
  #Required
  agreement_id    = oci_marketplace_listing_package_agreement.jdetrial_mkplace_package_agreement.agreement_id
  compartment_id  = lookup(oci_identity_compartment.jdetrial-compartment,"id")
  listing_id      = data.oci_marketplace_listing.jdetrial_mkplace.id
  package_version = data.oci_marketplace_listing.jdetrial_mkplace.default_package_version
  signature       = oci_marketplace_listing_package_agreement.jdetrial_mkplace_package_agreement.signature
}

resource "oci_marketplace_listing_package_agreement" "jdetrial_mkplace_package_agreement" {
  #Required
  agreement_id    = data.oci_marketplace_listing_package_agreements.jdetrial_mkplace_package_agreements.agreements[0].id
  listing_id      = data.oci_marketplace_listing.jdetrial_mkplace.id
  package_version = data.oci_marketplace_listing.jdetrial_mkplace.default_package_version
}



resource "oci_core_instance" "jdetrial_vm" {

  availability_domain = data.oci_identity_availability_domain.region_ad3.name
  fault_domain        = var.jdetrial_fault_domain
  compartment_id  = lookup(oci_identity_compartment.jdetrial-compartment,"id")
  display_name        = "oci_jdetrial_${var.environment}"
  shape               = var.instance_shape
  shape_config {
    baseline_ocpu_utilization = ""
    memory_in_gbs             = var.instance_shape_config_memory_in_gbs
    ocpus                     = var.instance_ocpus
  }
  create_vnic_details {
    #assign_private_dns_record = <<Optional value not found in discovery>>
    assign_public_ip = "true"
    display_name   = "app_vnic_jdetrial${var.environment}"
    freeform_tags = {
    }
    hostname_label = "jdetrial${var.environment}"
    skip_source_dest_check = "false"
    subnet_id              = oci_core_subnet.jdetrial_subnet_web.id
    #vlan_id = <<Optional value not found in discovery>>
  }

  metadata = {
    "ssh_authorized_keys" = var.instance_public_sshkey
    # user_data = base64encode(file("Meta Data script of your choosing"))
    user_data             = "${base64encode(file("./scripts/vm.cloud-config"))}"
    #user_data             = data.template_cloudinit_config.nodes.rendered
  }

  source_details {
    source_id   = data.oci_core_app_catalog_listing_resource_version.jdetrial_catalog_listing.listing_resource_id
    source_type = "image"
  }

  lifecycle {
    ignore_changes = [
      source_details[0].source_id,
    ]
  }
}

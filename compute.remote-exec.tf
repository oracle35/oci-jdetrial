/*Copyright © 2018, Oracle and/or its affiliates. All rights reserved.

The Universal Permissive License (UPL), Version 1.0*/


resource "random_integer" "rand" {
  min = 1000000000
  max = 9999999999
}

#locals {
#  remote_exec_script_enabled = "${var.remote_exec_script != "" ? 1 : 0}"
#}

resource "null_resource" "initjde" {
  depends_on  = [oci_core_instance.jdetrial_vm]
  
  provisioner "file" {
    connection {
      type               = "ssh"
      timeout             = "${var.timeout}"
      host                = oci_core_instance.jdetrial_vm.public_ip
      user                = "opc"
      private_key         = "${file("${var.instance_private_key}")}"
    }
    #source        = "scripts/jdedwards.template"
    #content        = base64gzip(local.setup_jdedwards)
    content        = local.setup_jdedwards
    destination   = "/home/opc/jdedwards"
  }

  provisioner "remote-exec" {
    connection {
      type               = "ssh"
      timeout             = "${var.timeout}"
      host                = oci_core_instance.jdetrial_vm.public_ip
      user                = "opc"
      private_key         = "${file("${var.instance_private_key}")}"
    }

    inline = [
       "sudo -i mv /home/opc/jdedwards /etc/sysconfig/jdedwards;",
       "sudo -i nohup /u01/vmScripts/EOne_Sync.sh -config > /home/opc/jde.out 2>&1 &",
       "sleep 1"
    ]
  }
}

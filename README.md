# ![JD Edwards Logo](./images/oci-jde.png)

This project is a Terraform configuration that deploys automatically the latest JDE Trial Edition on [Oracle Cloud Infrastructure][oci].


## Getting Started with jdetrial

The repository contains the [Terraform][tf] code to create all the required resources and configures the JDE Trial environment from start to end:

  * a compartment (named is defined in tfvars file)
  * a VCN with 1 Subnet and 1 Security List
  * a JDE Trial instance from the Market Place

### Prerequisites

You should have an OCI tenant and a user with Administrator privileges, an API Key created.


1. Clone the [oci-jdetrial](git-project) project

```bash
git clone https://gitlab.com/oracle35/oci-jdetrial.git
```

2. Configure your deployment by creating the file **terraform.tf** using the template file  **terraform.tfvars-template** and updating the variables based on your target configuration:

```shell
# OCI authentication
user_ocid         = "ocid1.user.oc1..aaxxxxxxxxxxxxxxxxxxxxxxxxx"
fingerprint       = "de:d7:b4:db:d9:97:xx:xx:xxx:xx:xx:xx:xx"
tenancy_ocid      = "ocid1.tenancy.oc1..aaxxxxxxxxxxxxxxxxxxxxxxxxx"
private_key_path  = "/path/.oci/oci_api_key.pem"
# region
region = "eu-frankfurt-1"

# jdeday Parent Compartment (could be tenant OCID)
compartment_parent_ocid = ""

# SSH Public key for JDE Trial Instance
instance_private_key = "<ENTER PATH TO PRIVATE KEY FILE>"
instance_public_sshkey = "<ENTER PUBLIC KEY VALUE>"

instance_shape = "VM.Standard.E4.Flex"

network_cidrs = {
    MAIN-VCN-CIDR                = "10.0.0.0/16"
    WEB-SUBNET-REGIONAL-CIDR     = "10.0.1.0/24"
    ALL-CIDR                     = "0.0.0.0/0"
    HOME-CIDR                    = "0.0.0.0/0"
  }

# Deploy WAF - Planned
use_waf = "true"

jdetrial_marketplace_name = "JD Edwards EnterpriseOne Trial Edition"
compartment_name = "comp-jdetrial"

# DB SYS Pwd should  be 8 to 10 char, +1 letter, +1 number, no $,|, @
jdedbsyspwd = "<ENTER PASSWORD>"
jdedbuserpwd = "<ENTER PASSWORD>"
jdewlspwd = "<ENTER PASSWORD>"
timeout = "30m"
```

5. Deploy the Terraform stack with the following commands:

```shell
terraform plan
terraform apply -auto-approve
```


> The application is being deployed to the compute instances asynchronously, and it may take a couple of minutes for the JDE configuration script to run. After 20-30 minutes, try to connect to the URL provided by the Terraform output:

```
"Your JDE Trial environment will be available in a few minutes at the following URL: http://xxx.xxx.xxx.xxx:8079/jde"
```

### Cleanup

If you want to terminate the application and remove all OCI resources, run the following Terraform command:

```shell
terraform destroy     # Enter yes to validate when prompted
```

#### Topology

The following diagram shows the topology created by this project.


# ![OCI-JDE Infra](./images/ocijdetrial-architecture.png)



[oci]: https://cloud.oracle.com/en_US/cloud-infrastructure
[oci-cost]: https://www.oracle.com/in/cloud/cost-estimator.html
[tf]: https://www.terraform.io
[git-project]: https://gitlab.com/oracle35/oci-jdetrial

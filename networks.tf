# Copyright (c) 2019, 2020 Oracle and/or its affiliates. All rights reserved.
# Licensed under the Universal Permissive License v 1.0 as shown at http://oss.oracle.com/licenses/upl.
#

resource "oci_core_virtual_network" "jdetrial_main_vcn" {
  cidr_block     = lookup(var.network_cidrs, "MAIN-VCN-CIDR")
  compartment_id  = lookup(oci_identity_compartment.jdetrial-compartment,"id")
  display_name   = "vcn_jdetrial_${var.environment}"
  dns_label      = "vcnps${var.environment}"
  freeform_tags  = local.common_tags
}

resource oci_core_default_dhcp_options jdetrial_dhcp_options {
  compartment_id  = lookup(oci_identity_compartment.jdetrial-compartment,"id")
  freeform_tags  = local.common_tags
  display_name     = "Default DHCP Options for JDE TRIAL VCN"
  domain_name_type = "CUSTOM_DOMAIN"
  manage_default_resource_id = oci_core_virtual_network.jdetrial_main_vcn.default_dhcp_options_id
  options {
    custom_dns_servers = [
    ]
    #search_domain_names = <<Optional value not found in discovery>>
    server_type = "VcnLocalPlusInternet"
    type        = "DomainNameServer"
  }
  options {
    #custom_dns_servers = <<Optional value not found in discovery>>
    search_domain_names = [
      "jdetrial.oraclevcn.com",
    ]
    #server_type = <<Optional value not found in discovery>>
    type = "SearchDomain"
  }
}

resource oci_core_internet_gateway jdetrial_internet_gateway {
  compartment_id  = lookup(oci_identity_compartment.jdetrial-compartment,"id")
  freeform_tags  = local.common_tags
  display_name   = "igw_jdetrial_${var.environment}"
  enabled        = "true"
  vcn_id = oci_core_virtual_network.jdetrial_main_vcn.id
}

resource "oci_core_service_gateway" "jdetrial_service_gateway" {
  compartment_id  = lookup(oci_identity_compartment.jdetrial-compartment,"id")
  freeform_tags  = local.common_tags
  display_name   = "sgw_jdetrial_${var.environment}"
  services {
    service_id = lookup(data.oci_core_services.all_services.services[0], "id")
  }
  vcn_id         = oci_core_virtual_network.jdetrial_main_vcn.id
}

resource oci_core_route_table jdetrial_internet_route_table {
  compartment_id  = lookup(oci_identity_compartment.jdetrial-compartment,"id")
  freeform_tags  = local.common_tags
  display_name   = "rt_jdetrial_pub_${var.environment}"
  route_rules {
    #description = <<Optional value not found in discovery>>
    destination       = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_internet_gateway.jdetrial_internet_gateway.id
  }
  vcn_id = oci_core_virtual_network.jdetrial_main_vcn.id
}


resource oci_core_subnet jdetrial_subnet_web {
  cidr_block                 = lookup(var.network_cidrs, "WEB-SUBNET-REGIONAL-CIDR")
  dhcp_options_id            = oci_core_virtual_network.jdetrial_main_vcn.default_dhcp_options_id
  compartment_id  = lookup(oci_identity_compartment.jdetrial-compartment,"id")
  display_name               = "sub_jdetrial-web${var.environment}"
  dns_label                  = "jdetrialweb${var.environment}"
  freeform_tags              = local.common_tags
  prohibit_internet_ingress  = "false"
  prohibit_public_ip_on_vnic = "false"
  route_table_id             = oci_core_route_table.jdetrial_internet_route_table.id
  security_list_ids = [
    oci_core_security_list.jdetrial_seclist_web.id
  ]
  vcn_id = oci_core_virtual_network.jdetrial_main_vcn.id
}

resource oci_core_security_list jdetrial_seclist_web {
  compartment_id  = lookup(oci_identity_compartment.jdetrial-compartment,"id")
  display_name               = "sl_jdetrial-web_${var.environment}"
  freeform_tags              = local.common_tags
  egress_security_rules {
    description      = "All all egress"
    destination      = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "6"
    stateless = "false"
  }
  egress_security_rules {
    #description = <<Optional value not found in discovery>>
    destination      = lookup(var.network_cidrs, "ALL-CIDR")
    destination_type = "CIDR_BLOCK"
    protocol  = "1"
    stateless = "false"
  }
  ingress_security_rules {
    description = "Allow HTTP traffic from Internet"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8079"
      min = "8079"
    }
  }
  ingress_security_rules {
    description = "Allow HTTPS traffic for JDE from Internet"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "8080"
      min = "8080"
    }
  }
  ingress_security_rules {
    description = "Allow HTTPS"
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "443"
      min = "443"
    }
  }
  ingress_security_rules {
    description = "Allow SSH"
    #icmp_options = <<Optional value not found in discovery>>
    protocol    = "6"
    source      = lookup(var.network_cidrs, "ALL-CIDR")
    source_type = "CIDR_BLOCK"
    stateless   = "false"
    tcp_options {
      max = "22"
      min = "22"
    }
  }
  vcn_id = oci_core_virtual_network.jdetrial_main_vcn.id
}


